# Deploy Android

Para realizar el deploy de la aplicación, existe dos formas: mediante [Capacitor o Cordova](https://ionicframework.com/docs/building/android). 

Utilizaremos Cordova para deployear directamente en un dispositivo con Android.

***Iniciaremos preparando el proyecto***
```
$ ionic cordova prepare android
```
![prepare](./imagenes-android/prepare.png)

***A continuación*** modificaremos el atributo id del elemento raíz, <"widget">.

```
Anterior
id="io.ionic.starter" 

Después
id="manticore.ejemplo" 
```

## **Correr la app directamente**

Con el siguiente comando levantamos la aplicación directamente en el dispositivo.

```
$ ionic cordova run android
```

![run](./imagenes-android/run.png)

Abrimos la aplicación desde el dispositivo para probarla.

![celular](./imagenes-android/celular.jpeg)

![app](./imagenes-android/app.jpeg)



<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>